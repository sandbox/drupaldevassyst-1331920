<?php
/**
 * @file
 * Additional theme Functions
 */

function phptemplate_settings($saved_settings) {

  $settings = theme_get_settings('assysttheme');

  $defaults = array(
    'assysttheme_style' => 'green',
    'assysttheme_width' => 0,
    'assysttheme_fixedwidth' => '960',
    'assysttheme_breadcrumb' => 0,
    'assysttheme_leftsidebarwidth' => '220',
    'assysttheme_rightsidebarwidth' => '220',
  );

  $settings = array_merge($defaults, $settings);

  $form['assysttheme_style'] = array(
    '#type' => 'select',
    '#title' => t('Style'),
    '#default_value' => $settings['assysttheme_style'],
    '#options' => array(
    'green' => t('Green'),
    'blue' => t('Blue'),
    'red' => t('Red'),
    ),
  );

  $form['assysttheme_width'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Fixed Width'),
    '#default_value' => $settings['assysttheme_width'],
  );

  $form['assysttheme_fixedwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Fixed Width Size'),
    '#default_value' => $settings['assysttheme_fixedwidth'],
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['assysttheme_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show Breadcrumbs'),
    '#default_value' => $settings['assysttheme_breadcrumb'],
  );

  $form['assysttheme_leftsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Left Sidebar Width'),
    '#default_value' => $settings['assysttheme_leftsidebarwidth'],
    '#size' => 5,
    '#maxlength' => 5,
  );

  $form['assysttheme_rightsidebarwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Right Sidebar Width'),
    '#default_value' => $settings['assysttheme_rightsidebarwidth'],
    '#size' => 5,
    '#maxlength' => 5,
  );

  return $form;
}


