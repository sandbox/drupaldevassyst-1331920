<?php
/**
 * @file
 * Displays a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the theme is located in, e.g. themes/garland or
 *   themes/garland/minelli.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or
 *   'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   element.
 * - $head: Markup for the HEAD element (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $body_classes: A set of CSS classes for the BODY tag. This contains flags
 *   indicating the current layout (multiple columns, single column), the
 *   current path, whether the user is logged in, and so on.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled in
 *   theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been
 *   disabled in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been
 *   disabled.
 * - $primary_links (array): An array containing primary navigation links for
 *   the site, if they have been configured.
 * - $secondary_links (array): An array containing secondary navigation links
 *   for the site, if they have been configured.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $left: The HTML for the left sidebar.
 * - $breadcrumb: The breadcrumb trail for the current page.
 * - $title: The page title, for use in the actual HTML content.
 * - $help: Dynamic help text, mostly for admin pages.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - $content: The main content of the current Drupal page.
 * - $right: The HTML for the right sidebar.
 * - $node: The node object, if there is an automatically-loaded node associated
 *   with the page, and the node ID is the second argument in the page's path
 *   (e.g. node/12345 and node/12345/revisions, but not comment/reply/12345).
 *
 * Footer/closing data:
 * - $feed_icons: A string of all feed icons for the current page.
 * - $footer_message: The footer message as defined in the admin settings.
 * - $footer : The footer region.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic
 *   content.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>">
<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
  <?php if (theme_get_setting('assysttheme_width')) { ?>
    <style type="text/css">
    #page {
      width : <?php print theme_get_setting('assysttheme_fixedwidth') ?>px;
    }
    </style>
  <?php } 
  else { ?>
    <style type="text/css">
    #page {
      width: 90%;
    }
    </style>
  <?php }  ?>
  <?php if ($left_sidebar_width = theme_get_setting('assysttheme_leftsidebarwidth')) { ?>
    <style type="text/css">
    body.sidebar-left #main {
      margin-left: -<?php print $left_sidebar_width ?>px;
    }
    body.sidebars #main {
      margin-left: -<?php print $left_sidebar_width ?>px;
    }
    body.sidebar-left #squeeze {
      padding-left: <?php print $left_sidebar_width ?>px;
    }
    body.sidebars #squeeze {
      padding-left: <?php print $left_sidebar_width ?>px;
    }
    #sidebar-left {
      width: <?php print $left_sidebar_width ?>px;
    }
    </style>
  <?php }  ?>
  <?php if ($right_sidebar_width = theme_get_setting('assysttheme_rightsidebarwidth')) { ?>
    <style type="text/css">
    body.sidebar-right #main {
      margin-right: -<?php print $right_sidebar_width ?>px;
    }
    body.sidebars #main {
      margin-right: -<?php print $right_sidebar_width ?>px;
    }
    body.sidebar-right #squeeze {
      padding-right: <?php print $right_sidebar_width ?>px;
    }
    body.sidebars #squeeze {
      padding-right: <?php print $right_sidebar_width ?>px;
    }
    #sidebar-right {
      width: <?php print $right_sidebar_width ?>px;
    }
    </style>
  <?php }  ?>
</head>
<body<?php print phptemplate_body_class($sidebar_left, $sidebar_right); ?>>
  <div id="page">
    <div id="header" class="clear-block">
      <div id="logo-title">
        <?php if ($logo): ?>
            <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"> <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo" /> </a>
        <?php endif; ?>
      </div><!-- /logo-title -->
      <div id="name-and-slogan">
        <?php if ($site_name): ?>
          <h1 class='site-name'> <a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"> <?php print $site_name; ?> </a> </h1>
        <?php endif; ?>
        <?php if ($site_slogan): ?>
          <div class='site-slogan'> <?php print $site_slogan; ?> </div>
        <?php endif; ?>
      </div><!-- /name-and-slogan -->
      <?php if ($header): ?>
        <div style="clear:both"></div>
        <?php print $header; ?>
      <?php endif; ?>
	  <?php print $search_box; ?>
    </div><!-- /header -->
	  <?php if (isset($primary_links) || isset($secondary_links)) { ?>
	<div id="menu">
        <div id="primarymenu">
        <?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
        <?php endif; ?>
        <?php if (isset($secondary_links)) : ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>  <?php endif; ?>
        </div>
	</div><!-- /menu -->
      <?php } ?>
	<?php if ($banner) { ?>
        <div id="main_banner"><?php print $banner ?> <div style="clear:both"></div>
		</div>
      <?php } ?>
    <div id="container">
      <?php if ($sidebar_left) { ?>
        <div id="sidebar-left"><?php print $sidebar_left ?> </div>
      <?php } ?>
      <div id="main">
        <div id="squeeze">
          <?php if (theme_get_setting('assysttheme_breadcrumb')): ?>
            <?php if ($breadcrumb): ?>
              <div id="breadcrumb"> <?php print $breadcrumb; ?> </div>
            <?php endif; ?>
          <?php endif; ?>
          <?php if ($mission) { ?>
            <div id="mission"><?php print $mission ?></div>
          <?php } ?>
            <div id="main-content">
             <?php if ($content_top):?>
                <div id="content-top"><?php print $content_top; ?></div>
				<div style="clear:both"></div>
              <?php endif; ?>
              <h1 class="title"><?php print $title ?></h1>
              <div class="tabs"><?php print $tabs ?></div>
  <?php print $help ?> <?php if ($show_messages) { 
  print $messages; } ?> <?php print $content; ?> <?php print $feed_icons; ?>
              <?php if ($content_middle): ?>
                <div id="content-middle"><?php print $content_middle; ?></div>
				<div style="clear:both"></div>
              <?php endif; ?>
              <?php if ($content_bottom): ?>
                <div id="content-bottom"><?php print $content_bottom; ?></div>
				<div style="clear:both"></div>
              <?php endif; ?>
            </div><!-- /main-content -->
        </div><!-- /squeeze -->
      </div><!-- /main -->
      <?php if ($sidebar_right) { ?>
        <div id="sidebar-right"><?php print $sidebar_right ?> </div>
      <?php } ?>	  <div style="clear:both"></div>
    </div><!-- /container -->
		<?php if ($richfooter_first || $richfooter_second || $richfooter_third || $richfooter_fourth  ) { ?>
        <div id="richfooter">
			<?php if ($richfooter_first) { ?>
				<div id="richfooter_first" class="richfooter-region"><?php print $richfooter_first ?> </div>
			<?php } ?>
			<?php if ($richfooter_second) { ?>
				<div id="richfooter_second" class="richfooter-region"><?php print $richfooter_second ?> </div>
			<?php } ?>
			<?php if ($richfooter_third) { ?>
				<div id="richfooter_third" class="richfooter-region"><?php print $richfooter_third ?> </div>
			<?php } ?>
			<?php if ($richfooter_fourth) { ?>
				<div id="richfooter_fourth" class="richfooter-region"><?php print $richfooter_fourth ?> </div>
			<?php } ?>
		 </div>
		  <div style="clear:both"></div>
      <?php } ?>
    <div id="footer">
      <?php if ($footer_region) { ?>
        <div id="footer-region"><?php print $footer_region?></div>
      <?php } ?>
      <?php if ($footer_message) { ?>
        <div id="footer-message"><?php print $footer_message ?></div>
      <?php } ?>
    </div><!-- /footer -->
  <div style="clear:both"></div>
  <?php print $closure ?>
  </div> <!-- /page -->
</body>
</html>
