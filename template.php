<?php
/**
 * @file
 * Overriding other theme functions
 */

function phptemplate_body_class($sidebar_left, $sidebar_right) {
  if ($sidebar_left != '' && $sidebar_right != '') {
  $class = 'sidebars';
  }
  else {
  if ($sidebar_left != '') {
  $class = 'sidebar-left';
  }
  if ($sidebar_right != '') {
  $class = 'sidebar-right';
  }
  }
  if (isset($class)) {
  print ' class="'. $class .'"';
}

}
if (is_null(theme_get_setting('assysttheme_style'))) {
  global $theme_key;
  // Save default theme settings
  $defaults = array(
    'assysttheme_style' => 'green',
    'assysttheme_width' => 0,
    'assysttheme_fixedwidth' => '960',
    'assysttheme_breadcrumb' => 0,
    'assysttheme_leftsidebarwidth' => '220',
    'assysttheme_rightsidebarwidth' => '220',
  );

  variable_set(
    str_replace('/', '_', 'theme_'. $theme_key .'_settings'),
    array_merge(theme_get_settings($theme_key), $defaults)
  );
  // Force refresh of Drupal internals
  theme_get_setting('', TRUE);
}

function get_assysttheme_style() {
  $style = theme_get_setting('assysttheme_style');
  if (!$style) {
    $style = 'green';
  }
  return $style;
}

$style = get_assysttheme_style();
drupal_add_css(drupal_get_path('theme', 'assysttheme') .'/css/'. $style .'.css', 'theme');


function phptemplate_menu_links($links, $attributes = array()) {

  if (!count($links)) {
    return '';
  }
  $level_tmp = explode('-', key($links));
  $level = $level_tmp[0];
  $output = "<ul class=\"links-$level ". $attributes['class'] ."\" id=\"". $attributes['id'] ."\">\n";

  $num_links = count($links);
  $i = 1;

  foreach ($links as $index => $link) {
    $output .= '<li';

    $output .= ' class="';
    if (stristr($index, 'active')) {
      $output .= 'active';
    }
    elseif ((drupal_is_front_page()) && ($link['href']=='<front>')) {
      $link['attributes']['class'] = 'active';
      $output .= 'active';
    }
    if ($i == 1) {
      $output .= ' first'; }
    if ($i == $num_links) {
      $output .= ' last'; }

    $output .= '"';

    $output .= ">". l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']) ."</li>\n";

    $i++;
  }
  $output .= '</ul>';
  return $output;
}
